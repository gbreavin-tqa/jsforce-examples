# README #

Examples of using jsforce for SOQL query, SOSL query, and executing anonymous Apex.

**Note: Executing anonymous apex doesn't return any debug log information.** Unfortunately this limits the usefulness of this feature, and it appears to be a limitation on the Salesforce side, not jsforce.

Clone this repository and then:

1. npm install
1. Enter your username and credentials into sfcreds.js
1. node index.js