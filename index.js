var jsforce = require('jsforce');
var sfcreds = require('./sfcreds');
var username = sfcreds.username;
var password = sfcreds.password;
var apexBody = `
    Account a = [select ID from Account limit 1];
    for(Contact c : [select ID, FirstName, LastName from Contact where AccountId = :a.ID]) {
        System.debug(c.ID + ', ' + c.FirstName + ' ' + c.LastName);
    }
`;

var conn = new jsforce.Connection({
  // you can change loginUrl to connect to sandbox or prerelease env.
  // loginUrl : 'https://test.salesforce.com'
});

conn.login(username, password, function(err, userInfo) {
  if (err) { return console.error(err); }

  f(conn);
});

var f = (conn) => {
    // example SOQL query - see: https://developer.salesforce.com/docs/atlas.en-us.204.0.soql_sosl.meta/soql_sosl/sforce_api_calls_soql.htm for reference
    conn.query("select ID from Account limit 1", (err, result) => {
        if (err) { return console.error("query error", err); }
        console.log("query results : " + JSON.stringify(result));
    });

    // example SOSL search - see: https://developer.salesforce.com/docs/atlas.en-us.204.0.soql_sosl.meta/soql_sosl/sforce_api_calls_sosl.htm for reference
    conn.search("FIND {INC} IN NAME FIELDS RETURNING Account(Id, Name)", (err, result) => {
        if (err) { return console.error("search error", err); }
        console.log("search results : " + JSON.stringify(result));
    });

    // example arbritrary Apex
    conn.tooling.executeAnonymous(apexBody, (err, result) => {
        if (err) { return console.error("anon. error", err); }
        console.log("anonymous result : " + JSON.stringify(result));
    });
}
